const menuToggle = document.querySelector('.menu-toggle');
const nav = document.querySelector('nav ul');
const navigasi = document.querySelector('.navigasi');

menuToggle.addEventListener('click',function(){
    nav.classList.toggle('slide');
});

navigasi.addEventListener('click',function(){
    nav.classList.remove('slide');
});
